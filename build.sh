#!/bin/sh
img=path-$(dirname $(readlink -f $0) | sha256sum | head -c7)
exec podman build . -t $img
