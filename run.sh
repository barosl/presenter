#!/bin/sh
img=path-$(dirname $(readlink -f $0) | sha256sum | head -c7)
exec podman run --rm -it -v $PWD:/app -w /app --net host $img uvicorn main:app --reload
