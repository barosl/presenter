async function main() {
  const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
  const recorder = new MediaRecorder(stream, { mimeType: 'audio/webm; codecs=opus' });
  recorder.addEventListener('dataavailable', async ev => {
    const audioData = new Blob([ev.data]);

    const body = new FormData();
    body.append('audio', audioData);
    await fetch('/api', {
      method: 'POST',
      body,
    });

    const el = document.createElement('audio');
    el.src = URL.createObjectURL(audioData);
    el.setAttribute('controls', '');
    document.body.appendChild(el);
  });

  const toggleEl = document.createElement('div');
  toggleEl.style.width = '200px';
  toggleEl.style.height = '200px';
  toggleEl.style.backgroundColor = 'green';
  function start(ev) {
    ev.target.setPointerCapture(ev.pointerId);
    toggleEl.style.backgroundColor = 'red';

    try {
      recorder.start();
    } catch (e) {
      alert('Error: ' + e);
    }
  }
  function stop(ev) {
    ev.target.releasePointerCapture(ev.pointerId);
    toggleEl.style.backgroundColor = 'green';

    try {
      recorder.stop();
    } catch (e) {
      alert('Error: ' + e);
    }
  }
  toggleEl.addEventListener('pointerdown', start);
  toggleEl.addEventListener('pointerup', stop);
  toggleEl.addEventListener('pointercancel', stop);
  document.body.appendChild(toggleEl);

  function connect() {
    console.log('connect()');
    const sock = new WebSocket(location.origin.replace(/^http/, 'ws') + '/ws');
    sock.addEventListener('message', ev => {
      console.log(ev.data);
    });
    sock.addEventListener('close', ev => {
      console.log('closed!');
      setTimeout(connect, 1_000);
    });
    sock.addEventListener('error', ev => {
      console.log('errored!');
    });
  }
  connect();
}

main();
