async function upload(ev) {
  const img = document.querySelector('#img').files[0];

  if (img) {
    const imgUrl = URL.createObjectURL(img);
    document.querySelector('#preview').src = imgUrl;
  } else {
    document.querySelector('#preview').src = 'img.jpg';
  }
  document.querySelector('#desc').textContent = '로드 중...';

  const body = new FormData();
  body.append('a', 'b');
  if (img) {
    body.append('img', img);
  }
  const resp = await fetch('/img', {
    method: 'POST',
    body,
  });
  const info = await resp.json();

  document.querySelector('#desc').textContent = info.text;
}
document.querySelector('#img').addEventListener('change', upload);
document.querySelector('#retry').addEventListener('click', upload);
upload();
