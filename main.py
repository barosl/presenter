#!/usr/bin/env python3

import openai
import tomllib
from pathlib import Path
from fastapi import FastAPI, UploadFile, WebSocket, WebSocketDisconnect, File
from fastapi.staticfiles import StaticFiles
import json
import asyncio
import base64
from typing import Annotated, Optional

app = FastAPI()
cfg = tomllib.loads(Path('cfg.toml').read_text())
cli = openai.OpenAI(api_key=cfg['api_key'])

socks = set()

async def broadcast(text):
    await asyncio.gather(*[sock.send_text(text) for sock in socks])

cur_slide = 1
num_of_slides = cfg['num_of_slides']
async def ask(text):
    global cur_slide
    print('text:', text)
    resp = cli.chat.completions.create(model='gpt-4-turbo-preview', messages=[
        {'role': 'system', 'content': f'매우 간략히 대답하세요. 총 슬라이드 개수는 {num_of_slides}개입니다. 현재 슬라이드는 {cur_slide}번째입니다.'},
        {'role': 'user', 'content': text},
    ], tools=[
        {
            'type': 'function',
            'function': {
                'name': 'next_slide',
                'parameters': {
                    'type': 'object',
                    'properties': {
                        'steps': {'type': 'number', 'description': 'The number of slides to skip'},
                    },
                    'required': ['steps'],
                },
            },
        },
        {
            'type': 'function',
            'function': {
                'name': 'prev_slide',
                'parameters': {
                    'type': 'object',
                    'properties': {
                        'steps': {'type': 'number', 'description': 'The number of slides to be rolled back'},
                    },
                    'required': ['steps'],
                },
            },
        },
    ])
    print('resp:', resp)
    msg = resp.choices[0].message
    if calls := msg.tool_calls:
        for call in calls:
            print('call:', call)
            if call.function.name in ['next_slide', 'prev_slide']:
                args = json.loads(call.function.arguments)
                key = 77 if call.function.name == 'next_slide' else 75
                sign = 1 if call.function.name == 'next_slide' else -1
                steps = args.get('steps', 1)
                cur_slide += sign * steps
                for _ in range(steps):
                    await broadcast(json.dumps({'key': key}))
                    await asyncio.sleep(0.1)
    else:
        print('content:', msg.content)

@app.post('/api')
async def api(audio: UploadFile):
    resp = cli.audio.transcriptions.create(model='whisper-1', file=('audio.webm', audio.file))
    await ask(resp.text)
    return '!!'

@app.websocket('/ws')
async def ws(sock: WebSocket):
    await sock.accept()
    socks.add(sock)

    while True:
        try:
            await sock.receive_text()
        except WebSocketDisconnect:
            print('sock disconnect')
            break

    return '??'

@app.post('/img')
def img_api(img: UploadFile | None = None):
    if img:
        img_buf = img.file.read()
        path = Path('uploads') / Path(img.filename).name
        path.parent.mkdir(parents=True, exist_ok=True)
        path.write_bytes(img_buf)
    else:
        img_buf = Path('static/img.jpg').read_bytes()
    resp = cli.chat.completions.create(model='gpt-4-vision-preview', messages=[
        {
            'role': 'user',
            'content': [
                {
                    'type': 'text',
                    'text': '이것은 무엇입니까?',
                },
                {
                    'type': 'image_url',
                    'image_url': {
                        #'url': 'https://static.wikia.nocookie.net/disgaea/images/5/54/DRPG_Madoka_Kaname_Artwork_1.png/revision/latest?cb=20211023133955&path-prefix=en',
                        #'url': 'https://m.media-amazon.com/images/M/MV5BMTU4MDUxMzcxMV5BMl5BanBnXkFtZTgwNzI5MDU2MDE@._V1_FMjpg_UX1000_.jpg',
                        'url': 'data:image/;base64,' + base64.b64encode(img_buf).decode('ascii'),
                    },
                },
            ],
        }
    ],
    max_tokens=1000)
    text = resp.choices[0].message.content
    print('text:', text)

#    resp = cli.chat.completions.create(model='gpt-4-turbo-preview', messages=[
#        {'role': 'system', 'content': f'Translate the following text into Korean:'},
#        {'role': 'user', 'content': text},
#    ])
#    text = resp.choices[0].message.content
#    print('text:', text)
    return {'text': text}

app.mount('/', StaticFiles(directory='static', html=True))

if __name__ == '__main__':
    main()
